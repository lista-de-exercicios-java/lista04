import br.edu.up.modelos.*;

public class App {
    public static void main(String[] args) throws Exception {

        String[] autores = new String[]{"Cay S.Horstmann" , "Gary Cornell", "Harvey Deitel"};

        Livros livro1 = new Livros("1598FHK","Core Java 2",autores, "01308193360", 2005);
        livro1.status();

        Livros livro2 = new Livros("9865PLO","Java, Como programar",autores, "0130341517", 2015);
        livro2.status2();
    }
}
