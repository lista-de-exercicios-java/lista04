package br.edu.up.modelos;

public class Livros {
    private String codigo;
    private String titulo;
    private String[] autores = new String[2];
    private String isbn;
    private int ano;

    public Livros(String codigo, String titulo, String[] autores, String isbn, int ano) {
        this.codigo = codigo;
        this.titulo = titulo;
        this.autores = autores;
        this.isbn = isbn;
        this.ano = ano;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String[] getAutores() {
        return autores;
    }

    public void setAutores(String[] autores) {
        this.autores = autores;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public int getAno() {
        return ano;
    }

    public void setAno(int ano) {
        this.ano = ano;
    }

    public void status(){
        System.out.println("o codigo do livro e: " + this.codigo);
        System.out.println("o titulo do seu livro e: " + this.titulo);
        System.out.println("o isbn e: " + this.isbn);
        System.out.println("o ano do seu livro e: " + this.ano);

        for(int i = 0; i < 2; i++){
            System.out.println("o titulo do seu livro e: " + this.autores[i]);
        }
        System.out.println("");
    }

    public void status2(){
        System.out.println("o codigo do livro e: " + this.codigo);
        System.out.println("o titulo do seu livro e: " + this.titulo);
        System.out.println("o isbn e: " + this.isbn);
        System.out.println("o ano do seu livro e: " + this.ano);
        System.out.println("o titulo do seu livro e: " + this.autores[3]);
        System.out.println("");
    }



    
}
